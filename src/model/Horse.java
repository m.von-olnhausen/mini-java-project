package model;

public class Horse {
    //Felder
    private int x;              //x-position of the horse
    private int y;              //y-position of the horse
    private float speedX;       //speed of the horse
    private String name;        //name of the horse ( for identification)

    //Konstruktor
    public Horse(int x, int y, float speedX, String name) {
        this.x = x;
        this.y = y;
        this.speedX = speedX;
        this.name = name;
    }

    //Methoden

    /**
     * updates the position of the horse
     *
     * @param elapdesTime the time that has already passed since starting
     */
    public void update(long elapdesTime) {
        this.x = Math.round(this.x + elapdesTime * this.speedX);
    }

    //Getter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return 20;
    }

    public int getW() {
        return 40;
    }

    public String getName() {
        return name;
    }

    public float getSpeedX() {
        return speedX;
    }
}