package model;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Model {

    //Felder
    public static final int WIDTH = 1400;   //Width of the game app
    public static final int HEIGHT = 700;   //Hight of the game app
    public boolean raceIsStarted;           //Tells, if the race has already started
    public boolean done;                    //Tells if the race has finished
    private Random random;                  //creates a random number
    private List<Horse> horses = new LinkedList<>();
    private String winner;                  //The name of the winner horse for the next race
    private String selectedHorse;           //The name of the horse you selected in the drop-down menu
    private boolean restarted;              //Says, if the game was already restarted

    //Konstruktor
    public Model() {
        restart();
    }

    //Methoden

    /**
     * updates the positions of the horses
     *
     * @param elapsedTime the time that has already passed
     */
    public void update(long elapsedTime) {
        if (raceIsStarted) {
            for (Horse horse : horses) {
                horse.update(elapsedTime);
            }
        }
    }

    /**
     * cleans the list of horses
     */
    public void cleanList() {
        for (Horse horse : horses) {
            horses.removeAll(horses);
        }
    }

    /**
     * restarts the race by cleaning the horse list, adding new horses and find out which the fastest horse is
     */
    public void restart() {
        cleanList();
        random = new Random();
        this.horses.add(new Horse(105, 150, random.nextFloat() / 1.5f, "Horse 1"));
        this.horses.add(new Horse(105, 200, random.nextFloat() / 1.5f, "Horse 2"));
        this.horses.add(new Horse(105, 250, random.nextFloat() / 1.5f, "Horse 3"));
        this.horses.add(new Horse(105, 300, random.nextFloat() / 1.5f, "Horse 4"));
        this.horses.add(new Horse(105, 350, random.nextFloat() / 1.5f, "Horse 5"));
        float speed = 0;
        for (Horse horse : horses) {
            if (horse.getSpeedX() > speed) {
                winner = horse.getName();
                speed = horse.getSpeedX();
            }
        }
        restarted = true;
    }

    /**
     * creates a list of the horses
     * @return returns this list
     */
    public List<String> horseList() {
        List<String> list = new LinkedList<>();
        for (Horse horse : horses) {
            list.add(horse.getName());
        }
        return list;
    }

    //Getter
    public String getWinner() {
        return winner;
    }

    public boolean isWon() {
        if (selectedHorse.equals(winner)) {
            return true;
        }
        return false;
    }

    public List<Horse> getHorses() {
        return horses;
    }

    public boolean getRaceIsFinished() {
        return done;
    }

    public boolean isRestarted() {
        return restarted;
    }

    //Setter
    public void setRaceIsStarted(boolean raceIsStarted) {
        this.raceIsStarted = raceIsStarted;
    }

    public void setRaceIsFinished(boolean raceFinished) {
        this.done = raceFinished;
    }

    public void setSelectedHorse(String selectedHorse) {
        this.selectedHorse = selectedHorse;
    }
}