import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseButton;
import model.Model;

public class InputHandler {

    //Felder
    private Model model;        //Creates a model of class Model

    //Konstruktor
    public InputHandler(Model model) {
        this.model = model;
    }

    //Methoden

    /**
     * handles the event of clicking the button
     *
     * @param button the button that was created in Main
     */
    public void onButtonClicked(Button button) {
        button.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                if (!model.getRaceIsFinished()) {
                    model.setRaceIsStarted(true);
                } else {
                    model.setRaceIsStarted(false);
                    model.setRaceIsFinished(false);
                    model.restart();
                }
            }
        });
    }

    /**
     * handles the event of selecting a horse
     * @param horsePicker the drop-down menu created in Main
     */
    public void selectHorse(ComboBox<String> horsePicker) {
        horsePicker.setOnAction(event -> {
            model.setSelectedHorse(horsePicker.getValue());
            if (model.isRestarted()) {
                model.setSelectedHorse(horsePicker.getValue());
            }
        });
    }
}