import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import model.Horse;
import model.Model;

public class Graphics {
    //Felder
    private Model model;
    private GraphicsContext gc;

    //Konstruktor
    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    //Methoden

    /**
     * paints the racing track, the horses and moves the pictures over time
     */
    public void draw() {
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);
        gc.setFill(Color.LAWNGREEN);
        gc.fillRect(80, 100, Model.WIDTH - 200, 300);
        gc.setFill(Color.WHITE);
        gc.fillRect(80, 125, Model.WIDTH - 200, 3);
        gc.fillRect(80, 175, Model.WIDTH - 200, 3);
        gc.fillRect(80, 225, Model.WIDTH - 200, 3);
        gc.fillRect(80, 275, Model.WIDTH - 200, 3);
        gc.fillRect(80, 325, Model.WIDTH - 200, 3);
        gc.fillRect(80, 375, Model.WIDTH - 200, 3);
        gc.setFill(Color.BLACK);
        gc.fillText("Welcome to the Horse Racing Track", Model.WIDTH / 2 - 100, 50);
        gc.fillRect(130, 100, 5, 300);
        gc.fillText("Start", 110, 80);
        gc.fillText("On which horse do you want to place your bet?", 30, 470);
        gc.fillRect(1200, 100, 5, 300);
        gc.fillText("Finish", 1190, 80);
        gc.fillText("Horse 1", 30, 155);
        gc.fillText("Horse 2", 30, 205);
        gc.fillText("Horse 3", 30, 255);
        gc.fillText("Horse 4", 30, 305);
        gc.fillText("Horse 5", 30, 355);
        gc.fillText("Winner:", Model.WIDTH / 2, 470);
        gc.fillText("Have you bet on the right horse?", Model.WIDTH / 2, 520);
        gc.fillText("Click to start the race", 410, 470);
        gc.fillText("Click again to get back into starting position", 355, 560);
        gc.fillText("1.", 150, 440);
        gc.fillText("2.", 465, 440);
        gc.setFill(Color.SADDLEBROWN);
        for (Horse horse : this.model.getHorses()) {
            if (horse.getX() >= Model.WIDTH - 150 - horse.getW() / 2) {
                gc.fillOval(
                        Model.WIDTH - 150 - (horse.getW() / 2),
                        horse.getY() - horse.getH() / 2,
                        horse.getW(),
                        horse.getH()
                );
                model.setRaceIsFinished(true);
            } else {
                gc.fillOval(
                        horse.getX() - horse.getW() / 2,
                        horse.getY() - horse.getH() / 2,
                        horse.getW(),
                        horse.getH()
                );
            }
        }
        //Paints the output of the race
        if (model.getRaceIsFinished()) {
            gc.setFill(Color.BLACK);
            gc.fillText("The winner of the race is " + model.getWinner(), Model.WIDTH / 2 + 250, 470);
            if (model.isWon()) {
                gc.setFill(Color.BLACK);
                gc.fillText("You won the bet! Congrats!", Model.WIDTH / 2 + 250, 520);
            } else if (!model.isWon()) {
                gc.setFill(Color.BLACK);
                gc.fillText("Sorry, you lost!", Model.WIDTH / 2 + 250, 520);
            }
        }
    }
}