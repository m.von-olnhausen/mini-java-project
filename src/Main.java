import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;
import model.Model;

public class Main extends Application {

    //Felder
    private Timer timer;

    //Methoden
    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Horse Racing");

        Model model = new Model();

        //Creating canvas, button, and combo-box(Drop-down menu)
        Canvas canvas = new Canvas(Model.WIDTH, Model.HEIGHT);
        Button button = new Button("Start/Reset");
        ComboBox<String> horsePicker = new ComboBox<>(FXCollections.observableArrayList(model.horseList()));
        horsePicker.setLayoutY(500);
        horsePicker.setLayoutX(100);
        horsePicker.setPromptText("Select a horse");
        button.setLayoutY(500);
        button.setLayoutX(430);

        Group group = new Group();
        group.getChildren().add(canvas);
        group.getChildren().add(horsePicker);
        group.getChildren().add(button);

        Scene scene = new Scene(group);
        stage.setScene(scene);
        stage.show();

        GraphicsContext gc = canvas.getGraphicsContext2D();

        Graphics graphics = new Graphics(model, gc);

        InputHandler inputHandler = new InputHandler(model);
        inputHandler.onButtonClicked(button);
        inputHandler.selectHorse(horsePicker);

        timer = new Timer(model, graphics);
        timer.start();
    }

    @Override
    public void stop() throws Exception {
        timer.stop();
        super.stop();
    }
}